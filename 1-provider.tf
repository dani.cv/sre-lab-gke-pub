provider "google" {
    project = "acn-tampa-atcdataai-capability"
    region = "us-central1"
}


terraform {
  backend "gcs" {
    bucket = "sre-curr-tf-state"
    prefix = "terraform/state"
  }

  required_providers {
    google = {
        source = "hashicorp/google"
        version = "~> 4.0"
    }
  }
}